/**
 * Created by mario on 02.11.14.
 */

var path = require('path');
var express = require('express');
var _ = require('lodash');
//var uuid = require('uuid');

/**
 * Create and expose router.
 */

var router = module.exports = new express.Router();

/**
 * Load initial data.
 */

var folders = require(path.join(__dirname, 'folders.json'));


// Handle id param.
//router.param('id', function (req, res, next, id) {
//    req.note = _.find(folders, {id: id});
//
//    if (!req.note) {
//        var error = new Error('Not Found');
//        error.status = 404;
//        return next(error);
//    }
//
//    next();
//});

/**
 * Filter that transforms GET and POST requests with _method
 * query parameter into PUT and DELETE
 */
router.all('*', function(req, res, next) {

    if(_.has(req.query, '_method')) {
        var method = req.query._method;
        req.method = method;
    }

    next();
});

// Find
router.get('/folders', function (req, res) {
    res.send(folders);
});

// Find by id
router.get('/folders/:id', function (req, res) {
    res.send(_.find(folders, { id: req.params.id }));
});

// Create
router.post('/folders', function (req, res) {
    var note = req.body;
    note.id = uuid();
    folders.push(note);
    res.send(201, note);
});

// Update
router.put('/folders/:id', function (req, res) {
    var folder = _.find(folders, { id: req.params.id });

    _.extend(folder, req.body);
    res.send(folder);
});

// Delete
router.delete('/folders/:id', function (req, res) {
    _.remove(folders, _.find(folders, { id: req.params.id }));
    res.send(204);
});

// Error handler.
router.use(function (err, req, res, next) {
    res.send(err.status || 500, {message: err.message});
});